const express = require('express');


const app = new express();

const PORT = 9000;

app.use('/api/products',(req,res)=>{
  res.send([
    { id: 1, name: 'Blue Clogs', description: 'A nice pair of blue clogs', price: '50.00'},
    { id: 2, name: 'Yellow Clogs', description: 'A nice pair of yellow clogs', price: '48.60'},
    { id: 3, name: 'Green Clogs', description: 'A nice pair of green clogs', price: '122.00'},
    { id: 4, name: 'Purple Clogs', description: 'A nice pair of purple clogs', price: '32.00'}
  ])
})
app.listen(PORT,()=>{
  console.log(`listening on port ${PORT}`)
})
